# Systemy pozycjonowania wewnątrz budynków 

## Opis
Repozytorium zawiera 4 foldery, które demostrują możliwości systemu pozycjonowania Pozyx.

- Projekt  ``pozyx`` zawiera projekt napisany w języku Python, który umożliwia nadawanie i odbieranie sygnału z Pozyxa poprzez multicast oraz w drugiej wersji, przy użyciu MQTT.
- Projekt ``PozyxGame`` zawiera grę w języku C# (Unity), która pokazuje możliwości Pozyxa. Gra zostanie zaprezentowana na konkursie IoT, który jest częścią finału konkursu Zespołowego Tworzenia Gier Komputerowych w 2019 roku.
- Projekt ``ProcessingSimulation`` zawiera graficzną symulację określania pozycji za pomocą Pozyxa, która jest odwzorowaniem oficjalnej aplikacji Pozyxa. Został napisany w języku Java przy użyciu biblioteki Processing.
- Folder ``parkingSimulation`` to projekt, który zbiera dane do pliku i umożliwia zwizualizowanie ich na wykresach.



## Środowisko

### Projekt ``pozyx``
- PyCharm Professional z [``paho-mqtt 1.4.0``](https://pypi.org/project/paho-mqtt/)
```
pip install paho-mqtt
```

### Projekt ``PozyxGame`` 
- Visual Studio Code lub Visual Studio lub Rider 

- Wymagany .NET Framework w wersji 4.6 lub wyższej oraz Unity w wersji 2018.2.xxfx.

### Projekt ``ProcessingSimulation``
- [Processing 3.x](https://processing.org) z domyślną konfiguracją programowania w Javie
- Rozszerzenie do Processing - [biblioteka wspierająca MQTT](https://docs.shiftr.io/manuals/processing/)

### Projekt ``ParkingSimulation``

- matlab/octave

### Uwagi
Systemy pozycjonowania można podłączyć do komputera, ale wskazane jest posiadanie RaspberryPi.
