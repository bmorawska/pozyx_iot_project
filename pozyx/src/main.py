from pypozyx import PozyxSerial, get_first_pozyx_serial_port, UWBSettings, DeviceCoordinates, Coordinates, \
    PozyxConstants, Acceleration, Temperature, Magnetic
from datetime import datetime
import PozyxData
import json
import paho.mqtt.client as mqtt
from time import sleep


broker_addr = "localhost"
collector = mqtt.Client("Bambus")
collector.connect(broker_addr)


# Połączenie z pozyxem
print("connecting...")
serial_port = get_first_pozyx_serial_port()
pozyx = PozyxSerial(serial_port)

if serial_port is not None:
    print("Connection success!")
else:
    print("No Pozyx port was found")

# Ustawienia UWB
uwb_settings = UWBSettings()
pozyx.getUWBSettings(uwb_settings)
print("UWB Settings: {}".format(uwb_settings))

# Ustawienia i stałe

pozyx.setPositionFilter(PozyxConstants.FILTER_TYPE_MOVING_AVERAGE, 10)

# Inicjalizacja kotwic
print("Anchors initializing...")

# device id, anchor or tag (1 or 0), coordinates (x, y, z)
anchors = [DeviceCoordinates(0x6757, 1, Coordinates(4395, 0,     1400)),
           DeviceCoordinates(0x6743, 1, Coordinates(0,    13175, 20)),
           DeviceCoordinates(0x6e04, 1, Coordinates(4395, 13175, 1330)),
           DeviceCoordinates(0x674c, 1, Coordinates(0,    0,     2000))]

status = True
for anchor in anchors:
    status = pozyx.addDevice(anchor)
    if status == PozyxConstants.STATUS_SUCCESS:
        print("Add Device - ok")
    else:
        print("Add Device - failure; {}".format(status))
        status = False

pozyx.saveNetwork()

if status:
    print("Anchors initialized")
else:
    print("Anchors initializing failure")


# Odczyt danych

dimension = PozyxConstants.DIMENSION_3D
height = 1500
algorithm = PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY

# Pozycja
position = Coordinates()
pozyx.doPositioning(position, dimension, height, algorithm)
print("Position: {}".format(position))

# Przyspieszenie
acceleration = Acceleration()
status = pozyx.getAcceleration_mg(acceleration)
if status == PozyxConstants.STATUS_SUCCESS:
    print("Acceleration:  ax={}, ay={}, az={}".format(acceleration.x, acceleration.y, acceleration.z))
else:
    print("Acceleration - failure; {}".format(status))

# Temperatura
temperature = Temperature()
status = pozyx.getTemperature_c(temperature)
if status == PozyxConstants.STATUS_SUCCESS:
    print("Temperature - ok. {} stopnie".format(temperature.byte_data))
else:
    print("Temperature - failure; {}".format(status))

# Pole magnetyczne
magnetic_field = Magnetic()
status = pozyx.getMagnetic_uT(magnetic_field)
if status == PozyxConstants.STATUS_SUCCESS:
    print("Magnetic Field - ok.  Pole magnetyczne wynosi {}".format(magnetic_field.byte_data))
else:
    print("Magnetic Field- failure; {}".format(status))

while True:
    positioningStatus = pozyx.doPositioning(position)
    status = pozyx.getAcceleration_mg(acceleration)
    x = PozyxData.PozyxData(positioningStatus, position, acceleration)
    y = json.dumps(x)
    collector.publish("Pozyx", payload=y)
    sleep(0.1)
    #print(y)


# while True:
#    now = datetime.now()
#    timestamp = datetime.timestamp(now)
#    positioningStatus = pozyx.doPositioning(position)
#    print(position)
#    status = pozyx.getAcceleration_mg(acceleration)
#    if status == PozyxConstants.STATUS_SUCCESS:
#        print("ax={}, ay={}, az={}".format(acceleration.x, acceleration.y, acceleration.z))
#    print("timestamp =", timestamp)
#    if positioningStatus == PozyxConstants.STATUS_SUCCESS:
#        print("success")
#    else:
#        print("failure")


# do jsona to
# przez mqtt wysyłka danych
















