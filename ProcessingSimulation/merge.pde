import peasy.*;
import mqtt.*;
import controlP5.*;

MQTTClient client;  //Klient MQTT
PeasyCam cam;       //Kamera
ControlP5 cp5;      //GUI

JSONObject json;
//Paleta kolorów
color blue = color (0,0,255);
color yellow = color (255,255,0);
color white = color (255, 255, 255);
color grey = color(200);
color darkGray = color(100);
PVector acv;

//Wszystkie odczytane punkty
ArrayList<PVector> points;
ArrayList<PVector> accelerationPoints;

String host = "mqtt://localhost:1883";

float rColor = 255;
float gColor = 0;
float bColor = 0;

boolean saveScreen = false;
String jsonFilename = "";
String screenFilename = "";


void setup() {
  size(1024,1024,P3D);
  //Ustawnienia kamery
  cam = new PeasyCam(this, 300);
  cam.setMinimumDistance(100);
  cam.setMaximumDistance(3000); 
  
  points = new ArrayList<PVector>();
  accelerationPoints = new ArrayList<PVector>();
  
  //Ustawienia klienta MQTT
  client = new MQTTClient(this);
  client.connect(host, "processing");
  println("client connected");
  client.subscribe("Pozyx");
  
  //GUI 
  setGUI();
}

void setGUI()
{
  PFont pfont = createFont("Arial",20,true);
  ControlFont font = new ControlFont(pfont, 241);
  
  cp5 = new ControlP5(this);
  cp5.addButton("Save points to JSON").setValue(0).setPosition(100, 950).setSize(300, 40).setId(1);
  cp5.addButton("Save current screen").setValue(1).setPosition(624, 950).setSize(300, 40).setId(2);
  cp5.addTextfield("Enter name for JSON file (without extension)").setValue(jsonFilename).setPosition(100, 910).setSize(300, 30).setId(3);
  cp5.addTextfield("Enter name for PNG file (without extension)").setValue(jsonFilename).setPosition(624, 910).setSize(300, 30).setId(4);
  cp5.addSlider("R").setPosition(800, 10).setSize(200, 20).setRange(0, 255).setValue(255).setColorCaptionLabel(color(255,0,0)).setColorForeground(color(255,0,0)).setColorActive(color(255,0,0));
  cp5.addSlider("G").setPosition(800, 35).setSize(200, 20).setRange(0, 255).setValue(0).setColorCaptionLabel(color(0,255,0)).setColorForeground(color(0,255,0)).setColorActive(color(0,255,0));
  cp5.addSlider("B").setPosition(800, 60).setSize(200, 20).setRange(0, 255).setValue(0).setColorCaptionLabel(color(0,0,255)).setColorForeground(color(0,0,255)).setColorActive(color(0,0,255));
  cp5.setAutoDraw(false); 
  cp5.getController("Save points to JSON").getCaptionLabel().setFont(font).toUpperCase(false).setSize(23);
  cp5.getController("Save current screen").getCaptionLabel().setFont(font).toUpperCase(false).setSize(23);
  cp5.getController("Enter name for JSON file (without extension)").getCaptionLabel().setFont(font).toUpperCase(false).setSize(11).setColor(0).setPadding(15, -45);
  cp5.getController("Enter name for JSON file (without extension)").getValueLabel().setFont(font).setSize(15);
  cp5.getController("Enter name for PNG file (without extension)").getCaptionLabel().setFont(font).toUpperCase(false).setSize(11).setColor(0).setPadding(15, -45);
  cp5.getController("Enter name for PNG file (without extension)").getValueLabel().setFont(font).setSize(15);
  cp5.getController("R").getCaptionLabel().setFont(font).setSize(14).setPadding(-215, 0);
  cp5.getController("G").getCaptionLabel().setFont(font).setSize(14).setPadding(-215, 0);
  cp5.getController("B").getCaptionLabel().setFont(font).setSize(14).setPadding(-215, 0);
}

//Odbieranie wiadomości z serwera przez mqtt
void messageReceived(String topic, byte[] payload) {
 // println("new message: " + topic + " - " + new String(payload));
  JSONObject json = parseJSONObject(new String(payload));
  //JSONObject json = parseJSONObject(example);
  if (json != null) {
     JSONArray coordinates = json.getJSONArray("coordinates");
     double[] tab = new double[3];
     JSONObject value = coordinates.getJSONObject(0);
     tab[0] = value.getDouble("x");
     tab[1] = value.getDouble("y");
     tab[2] = value.getDouble("z");
     
     tab[0] /= 20;
     tab[1] /= 20;
     tab[2] = (tab[2] - 1000) / 20;
     
     tab[1] = 250 - tab[1];
     
     PVector point = new PVector((int)tab[0], (int)tab[1], (int)tab[2]);
     points.add(point);
     println("Added point: " + tab[0] + " " + tab[1] + " " + tab[2]);
     
     
     JSONArray acceleration = json.getJSONArray("acceleration");
     double[] tabac = new double[3];
     JSONObject valueac = acceleration.getJSONObject(0);
     tabac[0] = valueac.getDouble("x");
     tabac[1] = valueac.getDouble("y");
     tabac[2] = valueac.getDouble("z");
     
     tabac[0] /= 100; 
     tabac[1] /= 100; 
     tabac[2] /= 100; 
     
     PVector a = new PVector((int)tabac[0], (int)tabac[1], (int)tabac[2]);
     acv = a;
     accelerationPoints.add(a);
     
     println("acc: " + acv.x + " " + acv.y + " " + acv.z);
  }
  else {
     println("Parsing failed");
  }
}

void draw() { 
 background(white);
  translate(-125,-125,0);
  
  drawChart();
  
  drawPointsOnChart();
  
  drawButtons();
  
}

void drawChart()
{
    pushMatrix();
  
    //Podłoga
    fill(grey);
    rect(0,-500,250,750);
 
    //Oś x
    stroke(0,100,0); 
    line(0, 0, 0, 260, 0, 0);
    line(260, 0, 0, 255, -3, 0);
    line(260, 0, 0, 255, 3, 0);
    fill(0,100,0);
    text("X Axis", 100, -5, 0);

    //Oś y
    stroke(255,0,0);
    line(0, 0, 0, 0, 260, 0);
    line(0, 260, 0, -3, 255, 0);
    line(0, 260, 0, 3, 255, 0);
    pushMatrix();
      rotate(-HALF_PI);
      fill(255,0,0);
      text("Y Axis",-150,-5,0);
    popMatrix();
 
    //Oś z
    stroke(0,0,255);
    line(0, 0, 0, 0, 0, 200);
    line(0, 0, 200, -3, 0, 195);
    line(0, 0, 200, 3, 0, 195);
    pushMatrix();
      rotateY(-HALF_PI);
      fill(0,0,255);
      text("Z Axis",100,-5,0);
    popMatrix();

    translate(0,0,50);
  popMatrix();
}

void drawPointsOnChart()
{
  //Rysowanie punktów
 for (int i = 0; i < points.size(); i++) {
    pushMatrix();
     pushMatrix();
      translate(points.get(i).x, points.get(i).y, points.get(i).z);
      noStroke();
      fill(rColor,gColor,bColor);
      sphere(2);
    popMatrix();
      
       //Oś x
    stroke(255,255,0); 
    line(points.get(i).x, points.get(i).y, points.get(i).z, points.get(i).x + accelerationPoints.get(i).x, points.get(i).y, points.get(i).z);

    //Oś y
    stroke(0,255,0);
    line(points.get(i).x, points.get(i).y, points.get(i).z, points.get(i).x, points.get(i).y + accelerationPoints.get(i).y, points.get(i).z);
 
    //Oś z
    stroke(0,0,255);
    line(points.get(i).x, points.get(i).y, points.get(i).z, points.get(i).x, points.get(i).y, points.get(i).z + accelerationPoints.get(i).z);
    popMatrix();
    
  }
}

//Event do guziczków
void controlEvent(ControlEvent theEvent) {
  int buttonID = theEvent.getController().getId();
  println("Pressed button with id: " + buttonID);
  switch(buttonID)
  {
    case(1):
      saveToJSON();
    break;
    case(2):
      saveScreen();
    break;
    case(3):
    if(theEvent.getController().getStringValue().length() >= 1)
      jsonFilename = theEvent.getController().getStringValue();
    else 
      jsonFilename = "default_chart";
      println("name for json file: " + jsonFilename);
    break;
    case(4):
    if(theEvent.getController().getStringValue().length() >= 1)
      screenFilename = theEvent.getController().getStringValue();
    else 
      screenFilename = "default_screen";
      println("name for screen file: " + screenFilename);
    break;
  }
}

void saveToJSON() {
  println("JSON here");
  JSONArray savedPoints = new JSONArray();
  
  int i = 0;
  for (PVector p: points)
  {
    JSONObject jsonPoint = new JSONObject();
    jsonPoint.setInt("point_id", i);
    jsonPoint.setFloat("x_pos", p.x);
    jsonPoint.setFloat("y_pos", p.y);
    jsonPoint.setFloat("z_pos", p.z);
    
    savedPoints.setJSONObject(i, jsonPoint);
    i++;
  }
  
  saveJSONArray(savedPoints, jsonFilename + ".json");
}

void saveScreen() {
  println("Screen here");
  saveScreen = true;
}

void R(float theValue)
{
  rColor = theValue;
}

void G(float theValue)
{
  gColor = theValue;
}

void B(float theValue)
{
  bColor = theValue;
}

//Tekst z przypieszeniem
void drawButtons() {
  if (saveScreen)
  {
    saveFrame("wykres-##.png");
    saveScreen = false;
  }
  hint(DISABLE_DEPTH_TEST);
  cam.beginHUD();
    cp5.draw();
  cam.endHUD();
  hint(ENABLE_DEPTH_TEST);
}
