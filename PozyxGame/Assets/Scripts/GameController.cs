﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    GameObject scoreManager;

    bool pause = false;

    public RectTransform gameOverPanel;
    public RectTransform winPanel;
    public Text timeUI;

    public float gameTime = 30.0f;
    float currentTime;
    public IEnumerator StartTimer()
    {
        while (currentTime > 0)
        {
            UpdateTimeUI();
            yield return new WaitForSeconds(0.1f);
            currentTime -= 0.1f;
        }

        if (currentTime <= 0)
        {
            GameOver();
        }
    }

    // Use this for initialization
    void Start () {
        scoreManager = GameObject.Find("ScoreManager");
        StartGame();

    }

    void UpdateTimeUI()
    {
        timeUI.text = currentTime.ToString("F1");
    }

    private void Update()
    {
        if (scoreManager.GetComponent<ScoreController>().AllCoinsGathered() && !pause)
        {
            StopCoroutine("StartTimer");
            GameWon();
        }
            
    }

    private void GameWon()
    {
        pause = true;
        winPanel.gameObject.SetActive(true);
        timeUI.gameObject.SetActive(false);
        DisablePlayer();
        scoreManager.GetComponent<ScoreController>().ShowWinScore(currentTime);
    }

    void GameOver()
    {
        pause = true;
        gameOverPanel.gameObject.SetActive(true);
        timeUI.gameObject.SetActive(false);
        DisablePlayer();
        scoreManager.GetComponent<ScoreController>().ShowGameOverScore();
    }

    public void Restart()
    {
        gameOverPanel.gameObject.SetActive(false);
        winPanel.gameObject.SetActive(false);
        timeUI.gameObject.SetActive(true);
        GameObject.Find("Room Generator").GetComponent<RoomGenerator>().GenerateCoins();
        GameObject.Find("Player").transform.position = new Vector3(4.0f, 1.0f, 4.0f);
        GameObject.Find("Player").GetComponent<Rigidbody>().velocity = Vector3.zero;
        scoreManager.GetComponent<ScoreController>().Restart();
        pause = false;
        EnablePlayer();
        StartGame();
    }

    void StartGame()
    {
        currentTime = gameTime;
        UpdateTimeUI();
        StartCoroutine("StartTimer");
    }

    void DisablePlayer()
    {
        GameObject.Find("Player").GetComponent<Rigidbody>().velocity = Vector3.zero;
        GameObject.Find("Player").GetComponent<PlayerController>().ChangeState();
        Camera.main.GetComponent<CameraController>().ChangeState();
    }

    void EnablePlayer()
    {
        GameObject.Find("Player").transform.position = new Vector3(4.0f, 1.0f, 4.0f);
        GameObject.Find("Player").transform.localEulerAngles = Vector3.zero;
        Camera.main.transform.localEulerAngles = Vector3.zero;
        GameObject.Find("Player").GetComponent<PlayerController>().ChangeState();
        Camera.main.GetComponent<CameraController>().ChangeState();
    }
}
