﻿using UnityEngine;
using System.Net.Sockets;
using System;
using System.Net;
using Assets.Scripts;
using SimpleJSON;

public class ClientUDP : MonoBehaviour
{

    private Socket sock;
    private IPEndPoint ipEndPoint;
    public string ip = "239.39.39.39";
    private IPAddress ipAddress;
    public int port = 5007;

    [HideInInspector]
    public float azimuth;
    [HideInInspector]
    public float elevation;
    [HideInInspector]
    public int positionX;
    [HideInInspector]
    public int positionY;
    [HideInInspector]
    public bool isPositioningSuccessful = false;

    private string message;

    // Use this for initialization
    void Start()
    {
        try
        {
            // https://docs.microsoft.com/en-us/dotnet/framework/network-programming/how-to-create-a-socket //
            sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //https://docs.microsoft.com/en-us/dotnet/api/system.net.ipaddress.parse?view=netframework-4.8//
            ipAddress = IPAddress.Parse(ip);
			Debug.Log("Adres = " + ipAddress.ToString() + "; port = " + port);
            ipEndPoint = new IPEndPoint(IPAddress.Any, port);

            // Bind endpoint to the socket
            sock.Bind(ipEndPoint);

            // Add socket to the multicast group
            // SocketOptionLevel - https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.socketoptionlevel?view=netframework-4.8
            // SocketOptionName - https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.socketoptionname?view=netframework-4.8
            // Multicast Option - https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.multicastoption?view=netframework-4.8
            sock.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ipAddress, IPAddress.Any));
			sock.Blocking = false;

        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }            
		Debug.Log("Siec sie uruchomila.");
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            byte[] data = new Byte[1024];
            sock.Receive(data);
            message = System.Text.Encoding.ASCII.GetString(data, 0, data.Length);
            //Debug.Log(message);

            JSONObject pozyxData = (JSONObject)JSON.Parse(message);
            isPositioningSuccessful = pozyxData["status"];
            positionX = pozyxData["coordinates"].AsArray[0]["x"];
            positionY = pozyxData["coordinates"].AsArray[0]["y"];
            azimuth = pozyxData["azimuth"];
            elevation = pozyxData["elevation"];

        }
		catch(SocketException e) 
		{
			//Debug.Log("Wyjątek " + e.ToString());
		}
         catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    private void OnDestroy()
    {
        // https://docs.microsoft.com/en-us/dotnet/framework/network-programming/using-a-synchronous-client-socket //
        sock.Close();
    }
}