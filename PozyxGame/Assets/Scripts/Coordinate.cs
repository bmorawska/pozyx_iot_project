﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    class Coordinate
    {
        public int x { get; set; }
        public int y { get; set; }
        public int z { get; set; }
    }
}
