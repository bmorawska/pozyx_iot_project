﻿namespace Assets.Scripts
{
    class Acceleration
    {
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
    }
}
