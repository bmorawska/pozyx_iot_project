﻿using System.Runtime.Serialization;

namespace Assets.Scripts
{
    [DataContract]
    class PozyxData
    {
        [DataMember]
        public double timestamp { get; set; }
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public Acceleration acceleration { get; set; }
        [DataMember]
        public Coordinate coordinates { get; set; }
        [DataMember]
        public double elevation { get; set; }
        [DataMember]
        public double azimuth { get; set; }
    }
}
