﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class RoomGenerator : MonoBehaviour {

    public Vector2 pozyxSize;
    public Vector2 roomSize;

    public Vector2 offset;
    public float wallHeight = 2.0f;

    public GameObject floor;
    public GameObject leftWall;
    public GameObject rightWall;
    public GameObject topWall;
    public GameObject bottomWall;

    public GameObject coinPrefab;

    public int numberOfCoins = 10;

    private readonly float positionModifier = 5.0f;
    private readonly float positionYModifier = 0.5f;
    private readonly float scaleModifier = 10.0f;
    private readonly float pozyxScaleModifier = 200.0f;
    private readonly float wallThickness = 0.1f;

    void Start () {
		
	}

    // Update is called once per frame
    void Update () {
		
	}

    public void GenerateRoom()
    {
        float unitSize = 10.0f;
        roomSize = new Vector2((pozyxSize.x + offset.x) / (pozyxScaleModifier * unitSize), (pozyxSize.y + offset.y) / (pozyxScaleModifier * unitSize));

        float translateFactorX = positionModifier * roomSize.x;
        float translateFactorZ = positionModifier * roomSize.y;


        floor.gameObject.transform.localScale = new Vector3(roomSize.x, 1.0f, roomSize.y);
        floor.gameObject.transform.position = new Vector3(translateFactorX, 0.0f, translateFactorZ);

        leftWall.gameObject.transform.position = new Vector3(-positionModifier * roomSize.x + translateFactorX,
                                                             positionYModifier * wallHeight, 
                                                             translateFactorZ);
        leftWall.gameObject.transform.localScale = new Vector3(wallThickness,
                                                               wallHeight,
                                                               scaleModifier * roomSize.y + 0.1f);

        rightWall.gameObject.transform.position = new Vector3(positionModifier * roomSize.x + translateFactorX,
                                                             positionYModifier * wallHeight,
                                                             translateFactorZ);
        rightWall.gameObject.transform.localScale = new Vector3(wallThickness,
                                                                wallHeight,
                                                                scaleModifier * roomSize.y + 0.1f);

        topWall.gameObject.transform.position = new Vector3(translateFactorX,
                                                            positionYModifier * wallHeight,
                                                            positionModifier * roomSize.y + translateFactorZ);
        topWall.gameObject.transform.localScale = new Vector3(scaleModifier * roomSize.x + 0.1f,
                                                              wallHeight,
                                                              wallThickness);

        bottomWall.gameObject.transform.position = new Vector3(translateFactorX,
                                                               positionYModifier * wallHeight,
                                                               -positionModifier * roomSize.y + translateFactorZ);
        bottomWall.gameObject.transform.localScale = new Vector3(scaleModifier * roomSize.x + 0.1f,
                                                                 wallHeight,
                                                                 wallThickness);
    }

    public void GenerateCoins()
    {
        GameObject[] remainingCoins = GameObject.FindGameObjectsWithTag("Coin");
        foreach (GameObject coin in remainingCoins)
        {
            DestroyImmediate(coin);
        }

        for (int i = 0; i < numberOfCoins; i++)
        {
            Vector3 coinPosition = new Vector3(Random.Range(0.5f, scaleModifier * roomSize.x - 1.0f), Random.Range(0.2f, 1.3f), Random.Range(0.5f, scaleModifier * roomSize.y - 1.0f));
            GameObject newCoin = Instantiate(coinPrefab, coinPosition, Quaternion.Euler(90, 90, 0));
            newCoin.transform.parent = GameObject.Find("Coins").transform;
        }
    }
}
