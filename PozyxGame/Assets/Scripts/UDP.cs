﻿using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;

public class UDP : MonoBehaviour
{

    private UdpClient client;
    private IPEndPoint localEp;
    private IPEndPoint RemoteEp;
    public string multicastIP = "239.39.39.39";
    public int port = 5007;
    private IPAddress multicastAddress;
    [HideInInspector]
    public string strData;

    // Use this for initialization
    void Start()
    {
        client = new UdpClient();
        client.ExclusiveAddressUse = false;
        localEp = new IPEndPoint(IPAddress.Any, port);
        client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

        client.Client.Bind(localEp);

        multicastAddress = IPAddress.Parse(multicastIP);
        client.JoinMulticastGroup(multicastAddress);

        client.Client.Blocking = false;

    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            byte[] data = client.Receive(ref RemoteEp);
            string strData = Encoding.Unicode.GetString(data);
            Debug.Log(strData);
        }
        catch (Exception e)
        {
            //Debug.Log(e.ToString());
        }
    }

    void OnDestroy()
    {
        client.DropMulticastGroup(multicastAddress);
        client.Close();
    }
}
