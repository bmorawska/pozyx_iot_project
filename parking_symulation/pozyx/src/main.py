from pypozyx import PozyxSerial, get_first_pozyx_serial_port, UWBSettings, DeviceCoordinates, Coordinates, \
    PozyxConstants, Acceleration, Temperature, Magnetic
import PozyxData
import json
import paho.mqtt.client as mqtt
from time import sleep
import math
import multicastTransmitter as multicast

#####
# Ustawienia urządzenia i komunikacji
#####

# Ustawienia MQTT
#broker_addr = "localhost"
#collector = mqtt.Client("Bambus")
#collector.connect(broker_addr)

# Połączenie z pozyxem
print("connecting...")
serial_port = get_first_pozyx_serial_port()
pozyx = PozyxSerial(serial_port)

if serial_port is not None:
    print("Connection success!")
else:
    print("No Pozyx port was found")

# Ustawienia UWB
uwb_settings = UWBSettings()
pozyx.getUWBSettings(uwb_settings)
print("UWB Settings: {}".format(uwb_settings))

# Ustawienia i stałe
pozyx.setPositionFilter(PozyxConstants.FILTER_TYPE_MOVING_AVERAGE, 10)

# Inicjalizacja kotwic
print("Anchors initializing...")

# device id, anchor or tag (1 or 0), coordinates (x, y, z)
anchors = [DeviceCoordinates(0x6757, 1, Coordinates(0,     4445, 1900)),
           DeviceCoordinates(0x6743, 1, Coordinates(1855,  4445, 2000)),
           DeviceCoordinates(0x6e04, 1, Coordinates(1855,  0,    2000)),
           DeviceCoordinates(0x674c, 1, Coordinates(0,     0,    1700))]

status = True
for anchor in anchors:
    status = pozyx.addDevice(anchor)
    if status == PozyxConstants.STATUS_SUCCESS:
        print("Add Device - ok")
    else:
        print("Add Device - failure; {}".format(status))
        status = False

pozyx.saveNetwork()

if status:
    print("Anchors initialized")
else:
    print("Anchors initializing failure")

dimension = PozyxConstants.DIMENSION_3D
height = 1500
algorithm = PozyxConstants.POSITIONING_ALGORITHM_UWB_ONLY


####
# Test odczytu danych
####

# Pozycja
position = Coordinates()
pozyx.doPositioning(position, dimension, height, algorithm)
print("Position: {}".format(position))

# Przyspieszenie
acceleration = Acceleration()
status = pozyx.getAcceleration_mg(acceleration)
if status == PozyxConstants.STATUS_SUCCESS:
    print("Acceleration:  ax={}, ay={}, az={}".format(acceleration.x, acceleration.y, acceleration.z))
else:
    print("Acceleration - failure; {}".format(status))

# Temperatura
temperature = Temperature()
status = pozyx.getTemperature_c(temperature)
if status == PozyxConstants.STATUS_SUCCESS:
    print("Temperature - ok. {} stopnie".format(temperature.byte_data))
else:
    print("Temperature - failure; {}".format(status))

# Pole magnetyczne
magnetic_field = Magnetic()
status = pozyx.getMagnetic_uT(magnetic_field)
if status == PozyxConstants.STATUS_SUCCESS:
    print("Magnetic Field - ok.  Pole magnetyczne wynosi {}".format(magnetic_field.byte_data))
else:
    print("Magnetic Field- failure; {}".format(status))


while True:

    # Obliczanie elewacji i azymutu na podstawie pola magnetycznego
    status = pozyx.getMagnetic_uT(magnetic_field)
    azymut = math.atan2(magnetic_field.x, magnetic_field.y)
    sqrroot = math.sqrt(magnetic_field.x * magnetic_field.x +
                        magnetic_field.y * magnetic_field.y +
                        magnetic_field.z * magnetic_field.z)

    elewacja = math.atan2(sqrroot, -magnetic_field.z)

    azymut *= 180 / math.pi
    azymut *= -1
    elewacja *= 180 / math.pi

    #yaw = azymut
    #roll = math.atan2(magnetic_field.z, magnetic_field.y)
    #pitch = math.atan2(magnetic_field.z, magnetic_field.x)

    #roll *= 180 / math.pi
    #pitch *= 180 / math.pi

    #print("yaw: %f, roll: %f, pitch: %f", (yaw, roll, pitch))

    # Określenie pozycji
    positioningStatus = pozyx.doPositioning(position)

    # Określenie przyspieszenia
    status = pozyx.getAcceleration_mg(acceleration)

    # Formatowanie danych do wysłania i wysłanie
    x = PozyxData.PozyxData(positioningStatus, position, acceleration, elewacja, azymut)
    y = json.dumps(x)
    #collector.publish("Pozyx", payload=y)

    # Częstotliwość nadawania
    #sleep(1)
    #print(y)

    multicast.send(str.encode(y))


