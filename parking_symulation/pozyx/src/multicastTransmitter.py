import socket

def send(message):
  MCAST_GRP = '239.39.39.39'
  #adress = '192.168.178.122'
  MCAST_PORT = 5007
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
  sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
  sock.sendto(message, (MCAST_GRP, MCAST_PORT))
  #sock.sendto(message, (adress, MCAST_PORT))


send(b'Hello World!')