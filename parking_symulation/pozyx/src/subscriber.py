import paho.mqtt.client as mqtt
import ssl
# libraries
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json


host = "localhost"
port = 1883
topic = "Pozyx"


def on_connect(client, userdata, flags, rc):
    print(mqtt.connack_string(rc))


# callback triggered by a new Pozyx data packet
def on_message(client, userdata, msg):
    print("Positioning update:", msg.payload.decode())

    s = msg.payload.decode()
    dict = json.loads(s)
#    coord = dict['coordinates']

#    x = coord['x']

#    ax.scatter(coord['x'], coord['y'], coord['z'], c='skyblue', s=60)
#    plt.show()


def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed to topic!")


#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#ax.view_init(30, 185)
client = mqtt.Client()

# set callbacks
client.on_connect = on_connect
client.on_message = on_message
client.on_subscribe = on_subscribe
client.connect(host, port=port)
client.subscribe(topic)

# works blocking, other, non-blocking, clients are available too.
client.loop_forever()
