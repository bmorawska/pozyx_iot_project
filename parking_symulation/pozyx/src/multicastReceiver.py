import socket
import struct
import json
import threading
import charts
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from time import sleep

#################
# Tables with data
#################

Xcords = []
Ycords = []

Xacc = []
Yacc = []

time = []

file = open('data.csv', 'w')
#file.write('timestamp')
#file.write(' ')
#file.write('coordinates - x')
#file.write(' ')
#file.write('coordinates - y')
#file.write(' ')
#file.write('acceleration - x')
#file.write(' ')
#file.write('acceleration - y')
#file.write(' ')
#file.write('acceleration - z')
#file.write('\n')

multicast_group = '239.39.39.39'
server_address = ('0.0.0.0', 5007)

# Create the socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind to the server address
sock.bind(server_address)

# Tell the operating system to add the socket to the multicast group
# on all interfaces.
group = socket.inet_aton(multicast_group)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

lock = threading.Lock();

counter = 0

while True:
    data, address = sock.recvfrom(1024)
    d = json.loads(data)
    try:
        Xcords.append(d['coordinates'][0]['x'])
        Ycords.append(d['coordinates'][0]['y'])

        if len(Xcords) % 15 == 0:
            plt.gca().set_xlim([-20, 2000])
            plt.gca().set_ylim([-20, 4500])
            plt.plot(Xcords, Ycords)
            plt.show()
            plt.savefig('./gif/' + str(counter) + '.png')
            counter = counter + 1

        file.write(str(d['timestamp']))
        file.write(',')
        file.write(str(d['coordinates'][0]['x']))
        file.write(',')
        file.write(str(d['coordinates'][0]['y']))
        file.write(',')
        file.write(str(d['acceleration'][0]['x']))
        file.write(',')
        file.write(str(d['acceleration'][0]['y']))
        file.write(',')
        file.write(str(d['acceleration'][0]['z']))
        file.write('\n')

    except KeyboardInterrupt:
        break
    print(d['coordinates'][0]['x'])

file.close()


