data = csvread ('data.csv');
timestamp = data(:, 1);
firsttime = timestamp(1);
timestamp = (timestamp - firsttime);

cordsX = data(:, 2);
cordsY = data(:, 3);
accX = data(:, 4);
accY = data(:, 5);
accZ = data(:, 6);

accX = accX / 1000;
accY = accY / 1000;
accZ = accZ / 1000;

anchorX = [0,     0, 20000, 20000];
anchorY = [0, 20000,  20000, 20000]; 

grid on;

ss=subplot(3,2,3);
hold on
plot(anchorX, anchorY, 'rx', 'MarkerSize', 10, 'LineWidth', 3)
scatter(cordsX, cordsY, 'filled')
hold off
title('Położenie')
xlabel('odległość od początku układu [mm]') 
ylabel('odległość od początku układu [mm]')


xlim(ss,[0 20000])
ylim(ss,[0 20000])

subplot(3,2,2);
plot(timestamp, accX, 'r', 'LineWidth', 1.5 )
title('Przyspieszenie w osi X')
ylabel('przyspieszenie [g = 9.81 m/s^2]')
xlabel('czas [s]')

subplot(3,2,4);
plot(timestamp, accY, 'g', 'LineWidth', 1.5 )
title('Przyspieszenie w osi Y')
ylabel('przyspieszenie [g = 9.81 m/s^2]') 
xlabel('czas [s]')

subplot(3,2,6);
plot(timestamp, accZ, 'b' , 'LineWidth', 1.5 )
title('Przyspieszenie w osi Z')
ylabel('przyspieszenie [g = 9.81 m/s^2]') 
xlabel('czas [s]')

par = get(ss,'position');
par(2)=0.11;
par(4)=0.8;
set(ss,'position',par);