from datetime import datetime

from pypozyx import PozyxConstants


def PozyxData(positioning_status, coordinates, acceleration, elevation, azimuth):
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    if positioning_status == PozyxConstants.STATUS_SUCCESS:
        status = "success"
    else:
        status = "failure"
    json = {"timestamp": timestamp,
            "status"   : status,
            "acceleration" : [
                {"x": acceleration.x,
                 "y": acceleration.y,
                 "z": acceleration.z
                 }
            ],
            "coordinates" : [
                {"x": coordinates.x,
                 "y": coordinates.y,
                 "z": coordinates.z
                 }
            ],
            "elevation": elevation,
            "azimuth"  : azimuth
            }
    return json


